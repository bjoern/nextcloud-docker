# nextcloud-docker

Spin up a complete Nextcloud Hub together with Nextcloud Office and Full-Text-Search.

**NOTE:** This is not meant for production! The whole setup was developed only for demo/testing purpose.

**NOTE:** The compose file works with both docker-compose and podman-compose

In order to access the Nextcloud and all additional services in the browser add following entries to your `/etc/hosts`:

````
127.0.0.1 nextcloud.local
127.0.0.1 collabora.local
127.0.0.1 elasticsearch.local
127.0.0.1 db.local
127.0.0.1 keycloak.local
````

Afterwards you can start the container directly `docker-compose up -d` and navigate to `http://nextcloud.local` in your browser.

## Initializing the container

You can configure a complete Nextcloud Hub system, including full text search by calling the `setup.sh` in the scripts folder. This way you don't have to configure everything manually.

For the mail setup (Nextcloud Mail and the system mails) some environment variables are required. You can add them to your `~/.bashrc`:

````
export NC_DOCKER_ROOT="/home/schiesbn/Repos/nextcloud/nextcloud-docker"
export NC_DEMO_MAIL_DOMAIN="domain of your email address used as sender for system mails"
export NC_DEMO_MAIL_FROM_ADDRESS="the part in front of the @-sign of your email address used as sender for system mails"
export NC_DEMO_MAIL_USER="user name for your email server"
export NC_DEMO_MAIL_ADDRESS="full email address used in the Mails app"
export NC_DEMO_MAIL_DISPLAY_NAME="your name used for mails you send with the Mails app"
export NC_DEMO_MAIL_PASSWD="password for your mail server"
export NC_DEMO_SMTP_HOST="smtp server"
export NC_DEMO_SMTP_PORT="587"
export NC_DEMO_SMTP_SSL="tls"
export NC_DEMO_IMAP_HOST="imap server"
export NC_DEMO_IMAP_PORT="993"
export NC_DEMO_IMAP_SSL="ssl"
````

run `source ~/.bashrc` to initialize the new variables before you run setup.sh.

You can also run the `setup.sh` without the environment variables, in this case the setup of the Mails app and the system mails will be skipped.

## Updating the full text search index

The index of the full text search will not be updated automatically. If you add or modify a file and want to index it, call

`occ fulltextsearch:index`

in the nextcloud container. Below you find some aliases, which will make it easier.

## Useful aliases

You don't have to use them. But it will make it a lot easier to operate the containers and the Nextcloud. I recommend to add it to your `~/.bashrc`:

```
# start/stop the container
alias nc-start='cd $NC_DOCKER_ROOT; docker-compose up -d; cd -'
alias nc-stop='cd $NC_DOCKER_ROOT; docker-compose down; cd -'
# rebuild the containers
alias nc-update-nextcloud='nc-stop; cd $NC_DOCKER_ROOT; docker rmi nextcloud-docker_nextcloud; docker-compose build --no-cache --pull nextcloud;cd -; nc-start'
alias nc-update-all='nc-stop; docker system prune -fa; cd $NC_DOCKER_ROOT; docker-compose build --no-cache --pull; cd -; nc-start'
# run a arbitrarily command in the nextcloud container
alias nc-exec='docker exec -it nextcloud '
# enter the nextcloud container
alias nc-enter='docker exec -it nextcloud bash'
# run occ commands from your host system
alias occ='docker exec -it nextcloud sudo -E -u www-data php /var/www/html/occ '
# enter the database
alias nc-db='docker exec -it nextcloud mycli nextcloud -h db -u nextcloud -p nextcloud'
# show nextcloud log
alias nc-log='docker exec -it nextcloud tail -f /var/www/html/data/nextcloud.log'
# reset a existing instance or initialize your instance for the first time
alias nc-reset-instance='$NC_DOCKER_ROOT/scripts/setup.sh'
# update the full-text-search index
alias nc-full-text-index='occ fulltextsearch:index'
# go to the volumne on the host system
alias nc-cd-volume='sudo su - root -c "cd /var/lib/docker/volumes/nextcloud-docker_nextcloud/_data; /bin/bash"'
```

run `source ~/.bashrc` to initialize the new aliases


# TODO

- [x] add keycloak for SSO (SAML + IODC)

