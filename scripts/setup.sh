#!/bin/bash

# regular used variables and commands
DB=nextcloud
DB_USER=nextcloud
DB_PASSWD=nextcloud
OCC='docker exec -it nextcloud sudo -E -u www-data php /var/www/html/occ -q'
DOCKER_EXEC='docker exec -it nextcloud'

# prepare database
echo -n "Preparing database... "
$DOCKER_EXEC mysql -e "drop database if exists nextcloud" --password=$DB_PASSWD
$DOCKER_EXEC mysql -e "create database nextcloud" --password=$DB_PASSWD
$DOCKER_EXEC mysql -e "create user if not exists 'nextcloud'@'localhost' identified by 'nextcloud'" --password=$DB_PASSWD
$DOCKER_EXEC mysql -e "grant all privileges on nextcloud.* to 'nextcloud'@'localhost'" --password=$DB_PASSWD
$DOCKER_EXEC mysql -e "flush privileges" --password=$DB_PASSWD
echo "Done."

# prepare webroot
echo -n "Preparing filesystem... "
$DOCKER_EXEC rm -rf /var/www/html/data
$DOCKER_EXEC mkdir /var/www/html/data
$DOCKER_EXEC rm /var/www/html/config/config.php
$DOCKER_EXEC touch /var/www/html/config/CAN_INSTALL
$DOCKER_EXEC chown -R www-data:www-data /var/www/html
$DOCKER_EXEC chmod -R g+rw /var/www/html
$DOCKER_EXEC chmod -R 770 /var/www/html/data
echo "Done."

# install Nextcloud
echo -n "Installing Nextcloud... "
$OCC maintenance:install --database "mysql" --database-host=db --database-name $DB  --database-user $DB_USER --database-pass $DB_PASSWD --admin-user "admin" --admin-pass "admin"
echo "Done."

# install Hub apps
echo -n "Installing Nextcloud Hub Apps... "
$OCC app:install richdocuments
$OCC app:install calendar
$OCC app:install contacts
$OCC app:install mail
$OCC app:install deck
$OCC app:install spreed
$OCC app:install groupfolders
$OCC app:install files_automatedtagging
$OCC app:install files_retention
$OCC app:install fulltextsearch
$OCC app:install fulltextsearch_elasticsearch
$OCC app:install files_fulltextsearch
$OCC app:install user_oidc
echo "Done."

# enable all apps
echo -n "Enable Nextcloud Hub Apps... "
$OCC app:enable richdocuments
$OCC app:enable calendar
$OCC app:enable contacts
$OCC app:enable mail
$OCC app:enable deck
$OCC app:enable spreed
$OCC app:enable groupfolders
$OCC app:enable files_automatedtagging
$OCC app:enable files_retention
$OCC app:enable fulltextsearch
$OCC app:enable fulltextsearch_elasticsearch
$OCC app:enable files_fulltextsearch
$OCC app:enable user_oidc
echo "Done."

# updating all apps
echo -n "Update all apps... "
$OCC app:update --all
echo "Done."

# add trusted domain
echo -n "Add trusted domain... "
$OCC config:system:set trusted_domains 1 --value=nextcloud.local
echo "Done."

# configure system mail
echo -n "Configure system mail... "
if [[ -z $NC_DEMO_SMTP_SSL || -z $NC_DEMO_MAIL_FROM_ADDRESS || -z $NC_DEMO_MAIL_DOMAIN || -z $NC_DEMO_SMTP_HOST || -z $NC_DEMO_SMTP_PORT || -z $NC_DEMO_MAIL_USER || -z $NC_DEMO_MAIL_PASSWD  ]]; then
  echo 'Failed. (Check the README.md for the necessary environment variables)'
else
  $OCC config:system:set mail_smtpmode --value=smtp
  $OCC config:system:set mail_smtpsecure --value=$NC_DEMO_SMTP_SSL
  $OCC config:system:set mail_sendmailmode --value=smtp
  $OCC config:system:set mail_from_address --value=$NC_DEMO_MAIL_FROM_ADDRESS
  $OCC config:system:set mail_domain --value=$NC_DEMO_MAIL_DOMAIN
  $OCC config:system:set mail_smtpauthtype --value=PLAIN
  $OCC config:system:set mail_smtpauth --value=1
  $OCC config:system:set mail_smtphost --value=$NC_DEMO_SMTP_HOST
  $OCC config:system:set mail_smtpport --value=$NC_DEMO_SMTP_PORT
  $OCC config:system:set mail_smtpname --value=$NC_DEMO_MAIL_USER
  $OCC config:system:set mail_smtppassword --value=$NC_DEMO_MAIL_PASSWD
  echo "Done."
fi

# setup mail app
echo -n "Configure Nextcloud mail... "
if [[ -z $NC_DEMO_MAIL_DISPLAY_NAME || -z $NC_DEMO_MAIL_ADDRESS || -z $NC_DEMO_SMTP_SSL || -z $NC_DEMO_IMAP_HOST || -z $NC_DEMO_IMAP_PORT || -z $NC_DEMO_IMAP_SSL || -z $NC_DEMO_SMTP_HOST || -z $NC_DEMO_SMTP_PORT || -z $NC_DEMO_MAIL_USER || -z $NC_DEMO_MAIL_PASSWD  ]]; then
  echo 'Failed. (Check the README.md for the necessary environment variables)'
else
    $OCC mail:account:create admin "$NC_DEMO_MAIL_DISPLAY_NAME" "$NC_DEMO_MAIL_ADDRESS" "$NC_DEMO_IMAP_HOST" $NC_DEMO_IMAP_PORT $NC_DEMO_IMAP_SSL "$NC_DEMO_MAIL_USER" "$NC_DEMO_MAIL_PASSWD" "$NC_DEMO_SMTP_HOST" $NC_DEMO_SMTP_PORT $NC_DEMO_SMTP_SSL "$NC_DEMO_MAIL_USER" "$NC_DEMO_MAIL_PASSWD"

  echo "Done."
fi

# configure Nextcloud Office
echo -n "Configuring Nextcloud Office... "
$OCC config:app:set --value "http://collabora:collabora@collabora.local:9980" richdocuments wopi_url
$OCC config:app:set --value "http://collabora:collabora@collabora.local:9980" richdocuments public_wopi_url
$OCC richdocuments:activate-config
echo "Done."

# configure full-text-search
echo -n "configuring full text search... "
$OCC config:app:set --value "1" fulltextsearch app_navigation
$OCC config:app:set --value "{\"deck\":\"1\",\"files\":\"1\"}" fulltextsearch provider_indexed
$OCC config:app:set --value "OCA\\FullTextSearch_Elasticsearch\\Platform\\ElasticSearchPlatform" fulltextsearch search_platform
$OCC config:app:set --value "standard" fulltextsearch_elasticsearch analyzer_tokenizer
$OCC config:app:set --value "http://elastic:elastic@elasticsearch.local:9200" fulltextsearch_elasticsearch elastic_host
$OCC config:app:set --value "my_index" fulltextsearch_elasticsearch elastic_index
#$OCC fulltextsearch:index
echo "Done."

# configure OIDC
echo -n "configuring OpenID-Connect... "
$OCC user_oidc:provider keycloak --clientid="nextcloud" --clientsecret="tZQK3AiWiiqsYpDGi3s3pQPYsRgHVoD0" --discoveryuri="http://keycloak.local:8080/realms/OIDCDemo/.well-known/openid-configuration"
#$OCC fulltextsearch:index
echo "Done."

# create additional test users
echo -n "create demo users... "
$OCC app:disable password_policy
curl -X PUT http://admin:admin@nextcloud.local/ocs/v1.php/cloud/users/admin -d key="email" -d value="$NC_DEMO_MAIL_ADDRESS" --header "OCS-APIRequest: true"  > /dev/null
curl -X POST http://admin:admin@nextcloud.local/ocs/v1.php/cloud/groups -d groupid="users" --header "OCS-APIRequest: true" > /dev/null
curl -X POST http://admin:admin@nextcloud.local/ocs/v1.php/cloud/users -d userid="user1" -d password="user1" -d groups[]="users" --header "OCS-APIRequest: true" > /dev/null
curl -X POST http://admin:admin@nextcloud.local/ocs/v1.php/cloud/users -d userid="user2" -d password="user2" -d groups[]="users" --header "OCS-APIRequest: true" > /dev/null
curl -X POST http://admin:admin@nextcloud.local/ocs/v1.php/cloud/users -d userid="user3" -d password="user3" -d groups[]="users" --header "OCS-APIRequest: true" > /dev/null
curl -X POST http://admin:admin@nextcloud.local/ocs/v1.php/cloud/users -d userid="user4" -d password="user4" -d groups[]="users" --header "OCS-APIRequest: true" > /dev/null
curl -X POST http://admin:admin@nextcloud.local/ocs/v1.php/cloud/users -d userid="user5" -d password="user5" -d groups[]="users" --header "OCS-APIRequest: true" > /dev/null
$OCC app:enable password_policy
echo "Done."

# Output list of available users
echo ""
echo "You can use the system now with following users:"
echo ""
echo "Admin user:" 
echo "  admin:admin"
echo ""
echo "Regular users:"
echo "  user1:user1"
echo "  user2:user2"
echo "  user3:user3"
echo "  user4:user4"
echo "  user5:user5"
echo ""
echo "OIDC User:"
echo "  oidcuser:oidcuser"
echo ""
echo "Go to http://nextcloud.local and have fun!"

